﻿# VOSS Automation Assessment

## Test Plan

### Purpose
This document describes the process followed in scripting and executing automated tests on the Ultimate QA website.

### Test Strategy
Write an automation script to complete the following actions:

1. Navigate to Ultimate QA Website
2. Capture a screenshot
3. Log in and log out
4. Browse to forms and complete forms
5. Browse to fake pricing page and purchase a basic package

### Tools
1. Tests have been written using the IntelliJ IDE. This can be downloaded from
```
https://www.jetbrains.com/idea/download/
```
2. Tests are to be executed on the Chrome and Firefox browsers.
3. Selenium will be required

### Risks

1. The link to buy packages on the website does not work, it just re-directs you to the top of the page

## Installation & Execution 

1. Clone project repo to desired location

```
git clone https://Carl_T84@bitbucket.org/Carl_T84/voss-assessment.git
```
2. Open project in deseired IDE

3. Run class Main.java




