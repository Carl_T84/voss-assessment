package com.voss;

public class TestData {

    private TestData() {
        throw new IllegalStateException("Utility Class");
    }

    public static final String WEBSITE_URL = "https://www.ultimateqa.com/automation/";
    public static final String PAGE_TITLE = "Automation Practice - Ultimate QA";
    public static final String USERNAME = "c.thomas@live.co.za";
    public static final String PASSWORD = "Lockdown!@345";
    public static final String FORM_NAME = "VOSS";
    public static final String FORM_TEXT = "Text goes here";
}