package com.voss;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BaseClass {

    public static WebElement getElement(WebDriver driver, By locator) {
        WebElement element = driver.findElement(locator);
        return element;
    }

    public static WebDriver launchBrowser(WebDriver driver, String browserName) {
        if (browserName.equals("chrome")) {
            System.setProperty("webdriver.chrome.driver", "chromedriver");
            driver = new ChromeDriver();
        } else if (browserName.equals("firefox")) {
            System.setProperty("webdriver.gecko.driver", "geckodriver");
            driver = new FirefoxDriver();
        } else {
            System.out.println("Incorrect Browser name" + browserName);
        }
        return driver;
    }
}