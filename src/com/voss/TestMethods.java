package com.voss;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.io.FileHandler;

import java.io.File;
import java.io.IOException;

public class TestMethods extends BaseClass {

    static WebDriver driver;

    public static void navigateToWebsite() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to(TestData.WEBSITE_URL);
    }

    public static String getPageTitle() {
        return driver.getTitle();
    }

    public static void captureScreenShot(WebDriver driver) {
        File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileHandler.copy(src, new File("./voss-screenshot" + System.currentTimeMillis() + ".png"));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void clickLoginAutomation() {
        By loginAutomationLink = By.linkText("Login automation");
        getElement(driver, loginAutomationLink).click();
    }

    public static void enterUsername() {
        By userNameField = By.id("user[email]");
        getElement(driver, userNameField).clear();
        getElement(driver, userNameField).sendKeys(TestData.USERNAME);
    }

    public static void enterPassword() {
        By passwordField = By.id("user[password]");
        getElement(driver, passwordField).clear();
        getElement(driver, passwordField).sendKeys(TestData.PASSWORD);
    }

    //
    public static void clickSignInButton() {
        By signInButton = By.xpath("//input[@value='Sign in']");
        getElement(driver, signInButton).click();
    }

    public static void signOut() {
        By dropDown = By.xpath("//a[contains(text(),'Carl T')]");
        getElement(driver, dropDown).click();

        By signOutButton = By.xpath("//a[contains(text(),'Sign Out')]");
        getElement(driver, signOutButton).click();
    }

    public static void clickForms() {
        driver.navigate().to(TestData.WEBSITE_URL);
        By fillOutFormsLink = By.linkText("Fill out forms");
        getElement(driver, fillOutFormsLink).click();
    }

    public static void submitFirstForm() {
        By enterNameOne = By.id("et_pb_contact_name_0");
        getElement(driver, enterNameOne).clear();
        getElement(driver, enterNameOne).sendKeys(TestData.FORM_NAME);

        By enterMessageOne = By.id("et_pb_contact_message_0");
        getElement(driver, enterMessageOne).clear();
        getElement(driver, enterMessageOne).sendKeys(TestData.FORM_TEXT);

        By clickSubmit = By.name("et_builder_submit_button");
        getElement(driver, clickSubmit).click();

    }

    public static void submitSecondForm() throws InterruptedException {

        By enterNameTwo = By.id("et_pb_contact_name_1");
        getElement(driver, enterNameTwo).clear();
        getElement(driver, enterNameTwo).sendKeys(TestData.FORM_NAME);

        By enterMessageTwo = By.id("et_pb_contact_message_1");
        getElement(driver, enterMessageTwo).clear();
        getElement(driver, enterMessageTwo).sendKeys(TestData.FORM_TEXT);

//        String captchaFirstValue = driver.findElement(By.className("input et_pb_contact_captcha")).getAttribute
//                ("data-first_digit");
//        String captchaSecondValue = driver.findElement(By.className("input et_pb_contact_captcha")).getAttribute
//                ("data-second_digit");
//        int firstInteger = Integer.parseInt(captchaFirstValue);
//        int secondInteger = Integer.parseInt(captchaSecondValue);
//        int sum = firstInteger+secondInteger;
//        String enterValue = String.valueOf(sum);
//
//        By enterCaptchaSum = By.id("et_pb_contact_captcha_1");
//        getElement(driver, enterCaptchaSum).clear();
//        getElement(driver, enterCaptchaSum).sendKeys(enterValue);

        By enterCaptchaSum = By.name("et_pb_contact_captcha_1");
        getElement(driver, enterCaptchaSum).clear();
        getElement(driver, enterCaptchaSum).sendKeys("18");

        By clickSubmit = By.name("et_builder_submit_button");
        Thread.sleep(5000);
        getElement(driver, clickSubmit).click();

    }

    public static void navigateToAutomationExercises() {
        By clickAutomationExercises = By.id("menu-item-587");
        getElement(driver, clickAutomationExercises).click();
    }

    public static void navigateToFakePricingPage() {
        By fakePricingPage = By.linkText("Fake Pricing Page");
        getElement(driver, fakePricingPage).click();
    }

    public static void purchasePlan() {
        By purchaseButton = By.xpath("(//a[@href='#top'])[2]");
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 500);");
        getElement(driver, purchaseButton).click();
    }
}