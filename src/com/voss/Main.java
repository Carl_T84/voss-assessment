package com.voss;

import org.testng.Assert;

public class Main extends TestMethods {

    public static void main(String[] args) throws InterruptedException {
        BaseClass.launchBrowser(driver,"chrome");
        navigateToWebsite();
        Assert.assertEquals(TestMethods.getPageTitle(), TestData.PAGE_TITLE);
        captureScreenShot(driver);
        clickLoginAutomation();
        enterUsername();
        enterPassword();
        clickSignInButton();
        signOut();
        clickForms();
        submitFirstForm();
        submitSecondForm();
        navigateToAutomationExercises();
        navigateToFakePricingPage();
        purchasePlan();
    }
}